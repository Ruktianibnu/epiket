package com.example.e_piket.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SessionManager {
    private static final String KEY_TOKEN = "tokenLogin";
    private static final String KEY_LOGIN = "isLogin";

    //untuk menyimpan session menggunakan shared preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    int PRIVATE_MODE = 0;
    Context context;
    String PREF_NAME = "LoginAppsEPiket";

    public SessionManager(Context c) {
        this.context = c;
        pref = c.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //membuat session login
    public void createLoginSession(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.putBoolean(KEY_LOGIN, true);
        editor.commit();
    }

    //mendapatkan TOKEN
    public String getToken() {
        return pref.getString(KEY_TOKEN, "");
    }

    public void setToken(String token){
        editor.putString("token", token);
        editor.commit();
    }

    //cek login
    public boolean isLogin() {
        return pref.getBoolean(KEY_LOGIN, false);
    }

    //logout user
    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    public String getNip() {
        return pref.getString("nip", "");
    }
    public void setNip(String nip){
        editor.putString("nip", nip);
        editor.commit();
    }

    public String getNama() {
        return pref.getString("nama", "");
    }
    public void setNama(String nama){
        editor.putString("nama", nama);
        editor.commit();
    }

    public String getKode_Subdirektorat() {
        return pref.getString("kode_subdirektorat", "");
    }
    public void setKode_Subdirektorat(String kode_subdirektorat){
        editor.putString("kode_subdirektorat", kode_subdirektorat);
        editor.commit();
    }

    public String getNama_Subdirektorat() {
        return pref.getString("nama_subdirektorat", "");
    }
    public void setNama_Subdirektorat(String nama_subdirektorat){
        editor.putString("nama_subdirektorat", nama_subdirektorat);
        editor.commit();
    }
}
