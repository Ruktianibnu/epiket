package com.example.e_piket.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.UUID;

public class AppsHelper {
    public static void ShowMessageInfo(String pesan, Context context) {
        MDToast mdToast = MDToast.makeText(context, pesan, Toast.LENGTH_LONG, MDToast.TYPE_INFO);
        mdToast.show();
    }

    public static void ShowMessageSuccess(String pesan, Context context) {
        MDToast mdToast = MDToast.makeText(context, pesan, Toast.LENGTH_LONG, MDToast.TYPE_SUCCESS);
        mdToast.show();
    }

    public static void ShowMessageError(String pesan, Context context) {
        MDToast mdToast = MDToast.makeText(context, pesan, Toast.LENGTH_LONG, MDToast.TYPE_ERROR);
        mdToast.show();
    }

    public static void ShowMessageWarning(String pesan, Context context) {
        MDToast mdToast = MDToast.makeText(context, pesan, Toast.LENGTH_LONG, MDToast.TYPE_WARNING);
        mdToast.show();
    }

    @SuppressLint("MissingPermission")
    public static String getDeviceUUID(Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        String deviceMobileNo = tm.getLine1Number();
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode()<<32 | tmSerial.hashCode()));
        return deviceUuid.toString().replace("_", "");
    }
}
