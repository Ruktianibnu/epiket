package com.example.e_piket.ActivityPartner;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.e_piket.ActivityInputData.ActivityInputData;
import com.example.e_piket.ActivityMenu.ActivityMenu;
import com.example.e_piket.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityPartner extends AppCompatActivity {

    @BindView(R.id.etNama)
    EditText etNama;
    @BindView(R.id.btnSearch)
    ImageButton btnSearch;
    @BindView(R.id.cd1)
    CardView cd1;
    @BindView(R.id.lvPegawai)
    ListView lvPegawai;
    @BindView(R.id.tvNip)
    TextView tvNip;
    @BindView(R.id.btnLanjut)
    Button btnLanjut;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnSearch, R.id.btnLanjut})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                break;
            case R.id.btnLanjut:
                intent = new Intent(ActivityPartner.this, ActivityInputData.class);
                startActivity(intent);
                break;
        }
    }
}
