package com.example.e_piket.ActivityMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_piket.ActivityPartner.ActivityPartner;
import com.example.e_piket.ActivityTamu.ActivityTamu;
import com.example.e_piket.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityMenu extends AppCompatActivity {

    @BindView(R.id.btnInputPusdakim)
    Button btnInputPusdakim;
    @BindView(R.id.cardView)
    CardView cvInputPusdakim;
    @BindView(R.id.btnDrc)
    Button btnDrc;
    @BindView(R.id.cardView5)
    CardView cvDrc;
    @BindView(R.id.btnVendor)
    Button btnVendor;
    @BindView(R.id.cardView2)
    CardView cvVendor;
    @BindView(R.id.btnJadwal)
    Button btnJadwal;
    @BindView(R.id.cardView4)
    CardView cvJadwal;
    @BindView(R.id.btnRiwayat)
    Button btnRiwayat;
    @BindView(R.id.cardView6)
    CardView cvRiwayat;
    @BindView(R.id.btnBantuan)
    Button btnBantuan;
    @BindView(R.id.cardView3)
    CardView cvBantuan;
    @BindView(R.id.rvPetugasPiket)
    RecyclerView rvPetugasPiket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnInputPusdakim, R.id.cardView, R.id.btnDrc, R.id.cardView5, R.id.btnVendor, R.id.cardView2, R.id.btnJadwal, R.id.cardView4, R.id.btnRiwayat, R.id.cardView6, R.id.btnBantuan, R.id.cardView3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnInputPusdakim:

                break;
            case R.id.cardView:
                break;
            case R.id.btnDrc:

                break;
            case R.id.cardView5:
                break;
            case R.id.btnVendor:

                break;
            case R.id.cardView2:
                break;
            case R.id.btnJadwal:

                break;
            case R.id.cardView4:
                break;
            case R.id.btnRiwayat:

                break;
            case R.id.cardView6:
                break;
            case R.id.btnBantuan:

                break;
            case R.id.cardView3:
                break;
        }
    }
}
