package com.example.e_piket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.e_piket.ActivityLogin.ActivityLogin;
import com.example.e_piket.ActivityMenu.ActivityMenu;
import com.example.e_piket.helper.BaseActivity;
import com.example.e_piket.helper.Constants;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*if(sesi.isLogin()) {
                    startActivity(new Intent(MainActivity.this, ActivityMenu.class));
                } else {
                    startActivity(new Intent(MainActivity.this, ActivityLogin.class));
                }*/

                startActivity(new Intent(MainActivity.this, ActivityLogin.class));
                finish();
            }
        }, 5000);
    }
}
