package com.example.e_piket.Network;

import com.example.e_piket.ActivityLogin.model.ResponseLogin;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiServices {
    @FormUrlEncoded
    @POST("login")
    Call<ResponseLogin> loginUser(
            @Field("nip") String Nip,
            @Field("password") String Password
    );
}
