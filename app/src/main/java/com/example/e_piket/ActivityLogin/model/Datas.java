package com.example.e_piket.ActivityLogin.model;

public class Datas{
	private String kodeSubdirektorat;
	private String namaSubdirektorat;
	private String nip;
	private String nama;

	public void setKodeSubdirektorat(String kodeSubdirektorat){
		this.kodeSubdirektorat = kodeSubdirektorat;
	}

	public String getKodeSubdirektorat(){
		return kodeSubdirektorat;
	}

	public void setNamaSubdirektorat(String namaSubdirektorat){
		this.namaSubdirektorat = namaSubdirektorat;
	}

	public String getNamaSubdirektorat(){
		return namaSubdirektorat;
	}

	public void setNip(String nip){
		this.nip = nip;
	}

	public String getNip(){
		return nip;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	@Override
 	public String toString(){
		return 
			"Datas{" + 
			"kode_subdirektorat = '" + kodeSubdirektorat + '\'' + 
			",nama_subdirektorat = '" + namaSubdirektorat + '\'' + 
			",nip = '" + nip + '\'' + 
			",nama = '" + nama + '\'' + 
			"}";
		}
}
