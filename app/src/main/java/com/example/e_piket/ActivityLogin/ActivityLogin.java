package com.example.e_piket.ActivityLogin;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.e_piket.ActivityLogin.model.Datas;
import com.example.e_piket.ActivityLogin.model.ResponseLogin;
import com.example.e_piket.ActivityMenu.ActivityMenu;
import com.example.e_piket.Network.NetworkClient;
import com.example.e_piket.R;
import com.example.e_piket.helper.AppsHelper;
import com.example.e_piket.helper.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends BaseActivity {

    @BindView(R.id.etNip)
    EditText etNip;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.btnLogin)
    Button buttonLogin;

    ResponseLogin responseLogin;
    ProgressDialog progressDialog;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int PERMISSION_REQUEST_READ_CONTACT = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        actionLogin();
        /*if (ContextCompat.checkSelfPermission(this,Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{
                    Manifest.permission.INTERNET,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_CAMERA_PERMISSION_CODE);
        }
        else
        {
            //checkInternet();
        }*/
    }

    private void actionLogin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_READ_CONTACT);
        }
        else
        {
            String nip = etNip.getText().toString();
            String password = etPassword.getText().toString();

            if (nip.equals("") || password.equals("")) {
                AppsHelper.ShowMessageError("nip dan password tidak boleh kosong", this);
            } else {
                progressDialog = new ProgressDialog(ActivityLogin.this);
                progressDialog.setMax(0);
                progressDialog.setMessage("Mohon Tunggu");
                progressDialog.setTitle("Sedang Mencoba Akses Server");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                progressDialog.show();

                try {
                    NetworkClient.service.loginUser(nip, password).enqueue(new Callback<ResponseLogin>() {
                        @Override
                        public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                            if (response.isSuccessful()) {
                                String pesan = response.body().getPesan();
                                Boolean result = response.body().isStatus();
                                String token = response.body().getToken();

                                if (result) {
                                    assert response.body() != null;
                                    Datas dataLogin = response.body().getDatas();
                                    sesi.setToken(token);
                                    sesi.setNip(dataLogin.getNip());
                                    sesi.setNama(dataLogin.getNama());
                                    sesi.setKode_Subdirektorat(dataLogin.getKodeSubdirektorat());
                                    sesi.setNama_Subdirektorat(dataLogin.getNamaSubdirektorat());
                                    sesi.createLoginSession(response.body().getToken());

                                    progressDialog.dismiss();
                                    activeonMoveActivity();
                                } else {
                                    AppsHelper.ShowMessageError(pesan, c);
                                    progressDialog.dismiss();
                                }
                            } else {
                                String pesan = response.body().getPesan();
                                AppsHelper.ShowMessageError(pesan, c);
                                progressDialog.dismiss();
                            }
                        }

                        private void activeonMoveActivity() {
                            Intent intent = new Intent(ActivityLogin.this, ActivityMenu.class);

                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(Call<ResponseLogin> call, Throwable t) {
                            String pesan = "Gagal Terhubung ke Server";
                            progressDialog.dismiss();
                            AppsHelper.ShowMessageError(t.toString(), c);
                        }
                    });
                } catch (Exception ex) {
                    checkInternet();
                    AppsHelper.ShowMessageInfo("Gagal Login", this);
                }
            }
        }
    }

    public boolean checkInternet(){
        boolean connectStatus = true;
        ConnectivityManager ConnectionManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true ) {
            connectStatus = true;
        }
        else {
            connectStatus = false;
            AppsHelper.ShowMessageInfo("No Internet Access", getApplicationContext());
        }
        return connectStatus;
    }
}
