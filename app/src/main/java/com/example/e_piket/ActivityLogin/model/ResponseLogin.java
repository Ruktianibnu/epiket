package com.example.e_piket.ActivityLogin.model;

public class ResponseLogin{
	private String pesan;
	private Datas datas;
	private String token;
	private boolean status;

	public void setPesan(String pesan){
		this.pesan = pesan;
	}

	public String getPesan(){
		return pesan;
	}

	public void setDatas(Datas datas){
		this.datas = datas;
	}

	public Datas getDatas(){
		return datas;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseLogin{" + 
			"pesan = '" + pesan + '\'' + 
			",datas = '" + datas + '\'' + 
			",token = '" + token + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
